# ESP32 Energy Monitor

### Summary
[Work in Progress] ESP-32 3-Phase Energy Monitor PCB and firmware using ADE9000 Energy Monitoring IC. 

### Tags
 - EAGLE
 - Hardware Project
 - DAQ

### Images
 - [Images/TopRendered.png] Rendered Board Render of finalized PCB
 - [Images/BottomRendered.png] Bottom rendered view of PCB

### Post 
