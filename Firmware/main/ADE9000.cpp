#include "ADE9000.h"

ADE9000::ADE9000(SPI* spi)
{
    this->spi = spi;
    gpio_set_direction(PIN_NUM_RST, GPIO_MODE_OUTPUT);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(10 / portTICK_RATE_MS);
    this->Reset();
    vTaskDelay(100 / portTICK_RATE_MS);
    this->Run();
    this->InitTemp();
    ESP_LOGI(LOG_TAG, "ADE9000 Initialized");
}

void ADE9000::Run(void)
{
    this->Write(ADDR_RUN,1);
}

void ADE9000::Reset(void)
{
    gpio_set_level(PIN_NUM_RST, 0);
    vTaskDelay(20 / portTICK_RATE_MS);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(20 / portTICK_RATE_MS);
    ESP_LOGI(LOG_TAG, "ADE9000 Reset");
}

uint16_t ADE9000::Read(uint16_t addr)
{
    // 16 bit read command
    uint16_t address = (addr & 0xFF00 >> 8) | (addr & 0xFF << 8);
    uint16_t commandHeader = ((address&0xFFF) << 4) | (1<<3) | (1 << 1);
    // Clear RX and TX SPI Buffer
    spi->ClearBuffers();
    // Send Command to ADE9000
    spi->Write(commandHeader,2);
    uint16_t value = (spi->rxBuffer[0]<<8) | (spi->rxBuffer[1]);
    return value;
}

uint32_t ADE9000::Read_32Bit(uint16_t addr)
{
    // 32 bit read command
    uint16_t address = (addr & 0xFF00 >> 8) | (addr & 0xFF << 8);
    uint16_t commandHeader = ((address&0xFFF) << 4) | (1<<3) | (1 << 1);
    // Clear RX and TX SPI Buffer
    spi->ClearBuffers();
    // Send Command to ADE9000
    spi->Write(commandHeader,4);
    uint32_t value = (spi->rxBuffer[0]<<24) | (spi->rxBuffer[1]<<16) | (spi->rxBuffer[2]<<8) | (spi->rxBuffer[3]);
    return value;
}

void ADE9000::Write(uint16_t addr, uint16_t value)
{
    // 16 bit write command
    uint16_t address = (addr & 0xFF00 >> 8) | (addr & 0xFF << 8);
    uint16_t commandHeader = ((address&0xFFF) << 4) | (1 << 1);
    // Clear RX and TX SPI Buffer
    spi->ClearBuffers();
    // Set Value
    spi->WriteTXBuffer(value);
    // Send Command to ADE9000
    spi->Write(commandHeader,2);
}

void ADE9000::Write_32Bit(uint16_t addr, uint32_t value)
{
    // 32 bit write command
    uint16_t address = (addr & 0xFF00 >> 8) | (addr & 0xFF << 8);
    uint16_t commandHeader = ((address&0xFFF) << 4) | (1 << 1);
    // Set Value
    spi->WriteTXBuffer32Bit(value);
    // Send Command to ADE9000
    spi->Write(commandHeader,4);
}

float ADE9000::MeasureVoltage(uint8_t phase)
{
    uint32_t addr = 1;
    switch(phase)
    {
        case 1: addr = ADDR_AVRMS; break;
        case 2: addr = ADDR_BVRMS; break;
        case 3: addr = ADDR_CVRMS; break;
    }
    // Read from Period register
    uint32_t result = this->Read_32Bit(addr); 
    float value = (result/52702092.0);
    return value;    
}

float ADE9000::MeasureCurrent(uint8_t phase)
{
    uint32_t addr = 1;
    switch(phase)
    {
        case 1: addr = ADDR_AIRMS; break;
        case 2: addr = ADDR_BIRMS; break;
        case 3: addr = ADDR_CIRMS; break;
    }
    // Read from Period register
    uint32_t result = this->Read_32Bit(addr); 
    float value = (result/52702092.0);
    return value;    
}

float ADE9000::MeasurePeriod(uint8_t phase)
{
    uint32_t addr = 1;
    switch(phase)
    {
        case 1: addr = ADDR_APERIOD; break;
        case 2: addr = ADDR_BPERIOD; break;
        case 3: addr = ADDR_CPERIOD; break;
    }
    // Read from Period register
    uint32_t result = this->Read_32Bit(addr);
    float value = ((result+1)/524288000.0);
    return value;    
}

float ADE9000::MeasureVAngle(uint8_t phase)
{
    uint32_t addr = 1;
    switch(phase)
    {
        case 1: return 0.0;
        case 2: addr = ADDR_ANGL_VA_VB; break;
        case 3: addr = ADDR_ANGL_VA_VC; break;
    }
    // Read from Period register
    uint32_t result = this->Read_32Bit(addr); 
    float value = result * 0.017578125;
    return value;    
}

float ADE9000::MeasureIAngle(uint8_t phase)
{
    uint32_t addr = 1;
    switch(phase)
    {
        case 1: addr = ADDR_ANGL_VA_IA; break;
        case 2: addr = ADDR_ANGL_VB_IB; break;
        case 3: addr = ADDR_ANGL_VC_IC; break;
    }
    // Read from Period register
    uint32_t result = this->Read_32Bit(addr); 
    float value = result * 0.017578125;
    return value;    
}

void ADE9000::InitTemp(void)
{
    // Enable Temp Sensor
    this->Write(ADDR_TEMP_CFG, (1<<2));
}

float ADE9000::MeasureTemp(void)
{   
    this->Write(ADDR_TEMP_CFG, (1<<2) | (1<<3));
    vTaskDelay(1 / portTICK_RATE_MS);
    uint32_t val = this->Read_32Bit(ADDR_TEMP_TRIM);
    uint16_t offset = (val & 0xFFFF0000) >> 16;
    uint16_t gain = (val & 0xFFFF);
    uint32_t result = this->Read(ADDR_TEMP_RSLT) & 0xFFF;
    float temp = (result * (gain / -65536.0)) + (offset/32.0);
    return temp / 100;
}