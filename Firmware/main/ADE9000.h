#ifndef ADE9000_H
#define ADE9000_H

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "ADE9000Reg.h"
#include "SPI.h"
#include "Pindef.h"

extern "C"
{
	#include "sdkconfig.h"
	#include "esp_log.h"
	#include "esp_system.h"
	#include "driver/spi_master.h"
	#include "esp_heap_caps.h"
	#include "freertos/FreeRTOS.h"
	#include "freertos/task.h"

    #include "soc/gpio_struct.h"
    #include "driver/gpio.h"
}

class ADE9000 {
private:
    SPI* spi;
public:
	ADE9000(SPI*);
    void Reset(void);
    void Run(void);
    void InitTemp(void);

    uint16_t Read(uint16_t);
    uint32_t Read_32Bit(uint16_t);
    void Write(uint16_t, uint16_t);
    void Write_32Bit(uint16_t, uint32_t);

    float MeasureTemp(void);
    float MeasurePeriod(uint8_t);
    float MeasureVoltage(uint8_t);
    float MeasureCurrent(uint8_t);
    float MeasureVAngle(uint8_t);
    float MeasureIAngle(uint8_t);

    const char* LOG_TAG = "ADE9000";
};

#endif /* ADE9000_H */
