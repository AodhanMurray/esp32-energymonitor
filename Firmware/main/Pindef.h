#ifndef PINDEF_H
#define PINDEF_H

#define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  19
#define PIN_NUM_CS   22

#define PIN_NUM_RST  GPIO_NUM_21

#endif /* PINDEF_H */