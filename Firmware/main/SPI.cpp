#include "SPI.h"

SPI::SPI(uint8_t MISO, uint8_t MOSI, uint8_t CLK) 
{
    // Initalize RX TX Buffers
    this->rxBuffer = (uint8_t*)heap_caps_malloc(rxLength, MALLOC_CAP_DMA);
    this->txBuffer = (uint8_t*)heap_caps_malloc(txLength, MALLOC_CAP_DMA);

    buscfg.miso_io_num = MISO;
    buscfg.mosi_io_num = MOSI;
    buscfg.sclk_io_num = CLK;
    buscfg.quadwp_io_num = -1;
    buscfg.quadhd_io_num = -1;

    //Initialize the SPI bus
    esp_err_t ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    assert(ret==ESP_OK);

}

void SPI::AddDevice(int CS, int freq, uint16_t command_bits, int queue_size)
{
    // Initialize Device
    devcfg.clock_speed_hz = freq;
    devcfg.mode = 0;
    devcfg.spics_io_num = CS;
    devcfg.command_bits = command_bits;
    devcfg.queue_size = queue_size;

    //Attach the Device to the SPI bus
    esp_err_t ret = spi_bus_add_device(HSPI_HOST, &devcfg, &this->spi);
    assert(ret==ESP_OK);
}


void SPI::Write(uint16_t command, int length)
{
	spi_transaction_t t = {};
    // Set Command Header 16 bit sent before data
    t.command = ((command & 0xFF00) >> 8) | ((command&0xFF)<<8);
    // Length of data to be sent to ESP-32
    t.length = length * 8;
    // Clear RX Buffer
    this->ClearRXBuffer();
    // TX Buffer to send data from
    t.tx_buffer = this->txBuffer;
    // RX Buffer to send data to
    t.rx_buffer = this->rxBuffer;
    esp_err_t ret = spi_device_transmit(spi, &t);
    assert(ret==ESP_OK);       
}

void SPI::ClearBuffers(void)
{
	this->ClearRXBuffer();
	this->ClearTXBuffer();
}

void SPI::WriteTXBuffer(uint16_t value)
{
	this->ClearTXBuffer();
	this->txBuffer[0] = (value & 0xFF00) >> 8;
	this->txBuffer[1] = value & 0xFF; 
}

void SPI::WriteTXBuffer32Bit(uint32_t value)
{
	this->ClearTXBuffer();
	this->txBuffer[0] = (value & 0xFF000000) >> 24;
	this->txBuffer[1] = (value & 0xFF0000) >> 16;
	this->txBuffer[2] = (value & 0xFF00) >> 8;
	this->txBuffer[3] = (value & 0xFF);
}

inline void SPI::ClearRXBuffer(void)
{
    // Clear RX Buffer
    memset(rxBuffer, 0, rxLength);
}

inline void SPI::ClearTXBuffer(void)
{
    // Clear TX Buffer
    memset(txBuffer, 0, txLength);
}