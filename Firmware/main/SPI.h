#ifndef SPI_H
#define SPI_H

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

extern "C"
{
	#include "sdkconfig.h"
	#include "esp_log.h"
	#include "esp_system.h"
	#include "driver/spi_master.h"
	#include "esp_heap_caps.h"
}

#include "Pindef.h"

class SPI {
private:
	inline void ClearRXBuffer(void);
	inline void ClearTXBuffer(void);
public:
	SPI(uint8_t MISO, uint8_t MOSI, uint8_t CLK);
	void AddDevice(int, int, uint16_t, int);
	void Write(uint16_t command, int length);

	void ClearBuffers(void);
	void WriteTXBuffer(uint16_t);
	void WriteTXBuffer32Bit(uint32_t);

	spi_device_handle_t spi = {};
    spi_bus_config_t buscfg = {};
    spi_device_interface_config_t devcfg = {};

	uint8_t* rxBuffer = 0;
	uint8_t* txBuffer = 0;
	uint32_t rxValue = 0;

	uint8_t rxLength = 8;
	uint8_t txLength = 8;
};

#endif /* SPI_H */
