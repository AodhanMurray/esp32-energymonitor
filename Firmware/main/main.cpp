#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern "C"
{
	#include "sdkconfig.h"

	#include "esp_log.h"
	#include "freertos/FreeRTOS.h"
	#include "freertos/task.h"

	void app_main();
}

#include "SPI.h"
#include "ADE9000.h"
#include "Pindef.h"

float version = 1.0;

const char* JSONFormat = "{\"VRMS\":[%.3f,%.3f,%.3f],\"VANG\":[%.3f,%.3f,%.3f],\"IRMS\":[%.3f,%.3f,%.3f],\"IANG\":[%.3f,%.3f,%.3f],\"FREQ\":[%.3f,%.3f,%.3f], \"Temp\": %.2f }\r\n";
char JSON[150];


void coreTask( void * pvParameters )
{ 
    while(true)
	{
		vTaskDelay(10 / portTICK_RATE_MS);
    }
}

void app_main() {
    ESP_LOGI("BOOT", "Energy Monitor Version %.1f\n",version);

	gpio_set_direction(GPIO_NUM_5, GPIO_MODE_OUTPUT);

	xTaskCreatePinnedToCore( coreTask,  "coreTask",  10000, NULL, 5,  NULL, 1);
 
	SPI spi(PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK);
	spi.AddDevice(PIN_NUM_CS, 1000000, 16, 1);

    vTaskDelay(50 / portTICK_RATE_MS);
	ADE9000 ade9000(&spi);
    vTaskDelay(100 / portTICK_RATE_MS);
	ade9000.Run();
	while (true) {

		float temp = ade9000.MeasureTemp();

		float V1 = ade9000.MeasureVoltage(1);
		float V2 = ade9000.MeasureVoltage(1);
		float V3 = ade9000.MeasureVoltage(1);

		float VANG1 = ade9000.MeasureVAngle(1);
		float VANG2 = ade9000.MeasureVAngle(2);
		float VANG3 = ade9000.MeasureVAngle(3);

		float I1 = ade9000.MeasureCurrent(1);
		float I2 = ade9000.MeasureCurrent(2);
		float I3 = ade9000.MeasureCurrent(3);

		float IANG1 = ade9000.MeasureIAngle(1);
		float IANG2 = ade9000.MeasureIAngle(2);
		float IANG3 = ade9000.MeasureIAngle(3);

		float P1 = ade9000.MeasurePeriod(1);
		float P2 = ade9000.MeasurePeriod(2);
		float P3 = ade9000.MeasurePeriod(3);

		float F1 = 1 / P1;
		float F2 = 1 / P2;
		float F3 = 1 / P3;

		sprintf(JSON,JSONFormat, V1, V2, V3, VANG1, VANG2, VANG3, I1, I2, I3, IANG1, IANG2, IANG3, F1, F2, F3, temp);
		printf(JSON);

    	vTaskDelay(20 / portTICK_RATE_MS);
	}
}
