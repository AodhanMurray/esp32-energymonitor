# Energy Monitor

### Summary
[Work in Progress] ESP-32 3-Phase Energy Monitor PCB and firmware using ADE9000 Energy Monitoring IC. 

### Rendered Board
| Top | Bottom |
|:----:|:----:|
|![Top Rendered Image](Images/TopRendered.png)|  ![Bottom Rendered Image](Images/BottomRendered.png)|
 
### Copper
| Top | Bottom |
|:----:|:----:|
|![Top Copper Image](Images/TopCopper.png)|  ![Bottom Copper Image](Images/BottomCopper.png)|